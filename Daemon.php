<?php

// email для отправки ошибок
const ERROR_EMAIL = '';

ini_set('error_log','daemon.log');

/**
 * Class Daemon
 *
 * @author S.Cheglakov
 */
class Daemon
{
    private $message = null;
    private $key = null;

    /**
     * Сколько секунд между запросами на обновление
     *
     * @var int
     */
    private $timeout = 6000;

    public function work()
    {
        error_log('Daemon start');

        try {
            // получим message и ключ для шифрования
            $this->getMessageAndKey();

            while (true) {
                // отсылаем запрос на обновление каждые $this->timeout секунд
                $this->sendUpdateRequest();

                error_log("Sleep for {$this->timeout} seconds");

                sleep($this->timeout);
            }

        } catch (Exception $e) {
            // отправим ошибку на почту
            mail(
                ERROR_EMAIL,
                'Daemon Error',
                "An error occured: {$e->getMessage()}\n"
                . "In {$e->getFile()}:{$e->getLine()}"
            );

            error_log('Exception catched, exit');
        }
    }

    /**
     * Получить сообщение и ключ для шифрования
     *
     * @throws Exception
     */
    private function getMessageAndKey()
    {
        $result = $this->sendRequest([
            'method' => 'get'
        ]);

        error_log('getMessageAndKey request response: ' . json_encode($result));

        if (!$result || empty($result['response']))
            throw new Exception('getMessageAndKey request failed, response: ' . json_encode($result));

        // запишем параметры
        $this->message = $result['response']['message'];
        $this->key = $result['response']['key'];
    }

    /**
     * Отправить запрос update
     *
     * @throws Exception
     */
    private function sendUpdateRequest()
    {
        $result = $this->sendRequest([
            'method' => 'update',
            'message' => base64_encode($this->message ^ $this->key)
        ]);

        error_log('Update request result: ' . json_encode($result));

        if (!$result || empty($result['response']) || $result['response'] !== 'Success') {
            throw new Exception('Update request failed, response: ' . json_encode($result));
        }
    }

    /**
     * Отправка запроса
     *
     * @param array $data
     * @return mixed
     */
    private function sendRequest($data = [])
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'https://syn.su/testwork.php');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $curlResponse = curl_exec($ch);
        curl_close($ch);

        return json_decode($curlResponse, true);
    }
}

// форкнем текущий процесс
$childPid = pcntl_fork();
if ($childPid === -1) {
    die('Error forking process' . PHP_EOL);
} elseif ($childPid) {
    // убить родительский процесс
    die();
}

$daemon = new Daemon();
$daemon->work();

// сделать дочерний процесс основным
posix_setsid();